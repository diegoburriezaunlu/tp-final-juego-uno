package ar.edu.poo.TPFinal.UNO.Vistas.VistaDeConsola;

import ar.edu.poo.TPFinal.UNO.Controladores.ControladorConsola;
import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.Color;
import ar.edu.poo.TPFinal.UNO.Modelos.Jugador;
import ar.edu.poo.TPFinal.UNO.Vistas.IVista;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class VistaConsola implements IVista {
    private final JFrame frame;
    private JPanel panel;
    private JTextArea textosalida;
    private JTextField textoEntrada;
    private JButton botonEnter;
    private ControladorConsola controladorConsola;
    private int cantCartasEnMano;
    private boolean esperaRespuesta;
    private boolean menuElejirColor;


    public VistaConsola(int jugador) {
        frame = new JFrame("<class name>");
        frame.setContentPane(this.panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setTitle("JUEGO DE CARTAS **** UNO ****   JUGADOR -->"+jugador);
        botonEnter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textosalida.append(textoEntrada.getText() + "\n");
                procesarEntrada(textoEntrada.getText());
                textoEntrada.setText("");
            }
        });
    }
    private void procesarEntrada(String input) {
        input = input.trim();
        if (input.isEmpty())
            return;
        if(this.esperaRespuesta){
            input=input.toUpperCase();
            switch (input) {
                case "S" -> {
                    this.esperaRespuesta=false;
                    controladorConsola.bajarUnaCarta(this.cantCartasEnMano-1); // Baja la carta que levanto
                }
                case "N" -> {
                    this.esperaRespuesta=false;
                    controladorConsola.pasarTurno(1);
                }
                default -> println("Opción inválida");
            }
        }else if (menuElejirColor) {
            switch (input) {
                case "1" -> {
                    this.menuElejirColor=false;
                    controladorConsola.setColorDeJuego("ROJO");
                    controladorConsola.pasarTurno(2);
                }
                case "2" -> {
                    this.menuElejirColor=false;
                    controladorConsola.setColorDeJuego("AMARILLO");
                    controladorConsola.pasarTurno(2);
                }
                case "3" -> {
                    this.menuElejirColor=false;
                    controladorConsola.setColorDeJuego("VERDE");
                    controladorConsola.pasarTurno(2);
                }
                case "4" -> {
                    this.menuElejirColor=false;
                    controladorConsola.setColorDeJuego("AZUL");
                    controladorConsola.pasarTurno(2);
                }
                default -> println("Opción inválida");
            }
        }else{
            if (esNumero(input)) { //si el input es numerico bajo una carta.
                if (((int) Integer.parseInt(input)) > 0 && ((int) Integer.parseInt(input)) <= cantCartasEnMano) {
                    controladorConsola.bajarUnaCarta(Integer.parseInt(input) - 1);
                } else {
                    println("Opción inválida");
                }
            } else { //si no es numerico, transformo a mayuscula y entro a un menu de opciones.
                input = input.toUpperCase();
                switch (input) {
                    case "U" -> controladorConsola.decirUno(); //Digo UNO (Ultima carta)
                    case "M" -> controladorConsola.tomoUnaCarta();//Tomo una carta del maso.
                    default -> println("Opción inválida");
                }
            }
        }
    }
    public void setMenuElejirColor(boolean set){
        this.menuElejirColor=set;
    }
    public void limpiarPantalla(){
        textosalida.setText("");
    }
    public void println(String texto) {
        textosalida.append(texto + "\n");
    }

    public void setCantCartasEnMano(int cantCartasEnMano) {
        this.cantCartasEnMano = cantCartasEnMano;
    }

    public void setControlador(ControladorConsola controladorConsola) {
        this.controladorConsola = controladorConsola;
    }
    public void mostrarTurno(String texto){
        println("******************************\n"+"Es el turno del "+texto+"\n"+"******************************");
    }
    public void dijoUno(String texto){
        println("\n#########################\n"+"¡¡"
                +texto+" dijo UNO!!"+"\n#########################\n");
    }
    public void mostrarMenuElejirColor(){
        println("\n#########################"
                +"\nElejir el color de mesa:"
                +"\nOpc 1 -> ROJO"+"\nOpc 2 -> AMARILLO"+"\nOpc 3 -> VERDE"+"\nOpc 4 -> AZUL"+"\n#########################\n");
    }
    public void mostrarMensaje(String texto){
        println("\n#########################\n"
                +texto+" -> S / N "+"\n#########################\n");
    }
    public void mostrarCartaALaVista(String texto){
        println("---------------------------------"+"\nCARTA EN MESA: \n"+texto+"\n"+"---------------------------------\n\n");
    }
    public void mostrarCartasMano(String texto){
        println("CARTAS EN MANO DEL JUGADOR: \n"+texto);
    }
    public void mostrarPuntos(String jugador1,String jugador2){
        println("PUNTAJE: \n"+"JUGADOR 1: "+jugador1+"\nJUGADOR 2: "+jugador2+"\n\n");
    }
    @Override
    public void mostrarMenuComandos() {
        println("\n\n********************************************\n" +
                    "U - CANTAR UNO \nM - TOMAR UNA CARTA DEL MAZO\n" +
                    "Elija la opcion para bajar una carta\n" +
                    "********************************************\n");
    }
    public void mostrarGanadorDelPartido(int jugador){
        println("\n\n\n\n\n\n*******************************\n" +
                    "EL GANADOR DEL PARTIDO ES:\n" +
                    "---> JUGADOR " + jugador+" <---\n"+
                    "*******************************\n");
    }
    public void mostrarColorEnJuego(Color color){
        println("\nCOLOR EN JUEGO: "+color+"\n");
    }
    public boolean esNumero(String texto) {
        boolean resultado;
        try {
            Integer.parseInt(texto);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
    public void setEsperaRespuesta() {
        this.esperaRespuesta = true;
    }
}
