package ar.edu.poo.TPFinal.UNO.Vistas;

import ar.edu.poo.TPFinal.UNO.Controladores.ControladorConsola;
import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.Color;

public interface IVista {
    public void setControlador(ControladorConsola controladorConsola);

    void mostrarTurno(String s);

    void dijoUno(String s);

    void mostrarCartaALaVista(String string);

    void mostrarCartasMano(String cartasManoDeJugador);

    void mostrarPuntos(String jugador1, String jugador2);
    void mostrarMenuComandos();
    public void setCantCartasEnMano(int cantCartasEnMano);
    public void mostrarColorEnJuego(Color color);
    public void mostrarMensaje(String texto);
    public void setEsperaRespuesta();
    public void limpiarPantalla();
    public void setMenuElejirColor(boolean set);
    public void mostrarMenuElejirColor();
    public void mostrarGanadorDelPartido(int jugador);
}
