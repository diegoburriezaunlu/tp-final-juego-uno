package ar.edu.poo.TPFinal.UNO.Controladores;

import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.Color;
import ar.edu.poo.TPFinal.UNO.Modelos.Mesa;
import ar.edu.poo.TPFinal.UNO.Modelos.ModeloJuego;
import ar.edu.poo.TPFinal.UNO.Observer.IObservador;
import ar.edu.poo.TPFinal.UNO.Vistas.IVista;

public class ControladorConsola implements IObservador {
    private IVista vista;
    private ModeloJuego modelo;
    private int jugador;
    private int ganadorDelPartido;
    public ControladorConsola(IVista vista, ModeloJuego modelo, int jugador) {
        this.vista = vista;
        vista.setControlador(this);
        this.modelo = modelo;
        this.jugador=jugador;
        this.ganadorDelPartido=99;
        modelo.agregarObservador(this);
    }

    @Override
    public void actualizar(Mesa mesa) {
        //limpio pantalla
        vista.limpiarPantalla();
        //Si hay un ganador del partido (Lo muestra en pantalla)
        if(mesa.getGanadorDelPartido()!=99){
            vista.mostrarGanadorDelPartido(mesa.getGanadorDelPartido());
            this.ganadorDelPartido=mesa.getGanadorDelPartido();
        }else {
            //Muestro de quien es el turno
            vista.mostrarTurno(mesa.getTurno());

            //Muestro los puntos acumulados de los dos jugadores
            vista.mostrarPuntos(String.valueOf(mesa.getPuntosDeJugadores(0)), String.valueOf(mesa.getPuntosDeJugadores(1)));

            //En caso que algun jugador haya dicho UNO, va a aparecer en pantalla.
            if (mesa.getUno() != null) {
                vista.dijoUno(mesa.getUno());
            }
            //Muestro carta a la vista en mesa
            vista.mostrarCartaALaVista(mesa.getCartaALaVista().mostrarCarta());

            //Muestro las cartas en mano del jugador correspondiente
            String texto = "";
            for (int i = 0; i < mesa.getCartasManoDeJugador(jugador - 1).size(); i++) {
                int j = i + 1;
                texto += "Opc " + j + " --> " + mesa.getCartasManoDeJugador(jugador - 1).get(i).mostrarCarta() + "\n";
            }
            vista.mostrarCartasMano(texto);

            //Mostrar color en juego.
            vista.mostrarColorEnJuego(mesa.getColorEnJuego());

            //En caso que algun jugador tenga un mensaje si baja lo que tomo del mazo, va a aparecer en pantalla.
            if (mesa.getMensajesAJugadoresSiBajaCarta() != null && mesa.getaQueJugadorVaDirigido() == this.jugador) {
                vista.setEsperaRespuesta();
                vista.mostrarMensaje(mesa.getMensajesAJugadoresSiBajaCarta());
            } else if (mesa.getPedirEleccionColorMesa() && mesa.getaQueJugadorVaDirigido() == this.jugador) {
                vista.setMenuElejirColor(true);
                vista.mostrarMenuElejirColor();
                modelo.setPedirColorDeMesa(false);
            } else {
                //Muestra al final de la pantalla los comandos
                vista.mostrarMenuComandos();
            }
            vista.setCantCartasEnMano(mesa.getCantCartasEnMano(jugador));
        }
    }
    public int getJugador() { return jugador; }
    public void decirUno(){
        //Si hay un ganador del partido (Lo muestra en pantalla)
        if(this.ganadorDelPartido!=99){
            vista.mostrarGanadorDelPartido(this.ganadorDelPartido);
        }else {
            modelo.decirUno(getJugador());
        }
    }
    public void bajarUnaCarta(int carta) {
        //Si hay un ganador del partido (Lo muestra en pantalla)
        if (this.ganadorDelPartido != 99) {
            vista.mostrarGanadorDelPartido(this.ganadorDelPartido);
        } else {
            modelo.bajarUnaCarta(this.jugador, carta);
        }
    }
    public void tomoUnaCarta() {
        //Si hay un ganador del partido (Lo muestra en pantalla)
        if (this.ganadorDelPartido != 99) {
            vista.mostrarGanadorDelPartido(this.ganadorDelPartido);
        } else {
            modelo.tomarUnaCartaDeLaPila(this.jugador);
        }
    }
    public void pasarTurno(int cantudadDeTurnos){
        modelo.pasarTurno(cantudadDeTurnos);
    }
    public void setColorDeJuego(String color){
        modelo.setColorEnJuego(color);
    }
}
