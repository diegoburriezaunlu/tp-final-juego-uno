package ar.edu.poo.TPFinal.UNO.Observer;

import ar.edu.poo.TPFinal.UNO.Modelos.Mesa;

public interface IObservador {
    public void actualizar(Mesa mesa);

}
