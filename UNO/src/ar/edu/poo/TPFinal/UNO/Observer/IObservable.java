package ar.edu.poo.TPFinal.UNO.Observer;

import ar.edu.poo.TPFinal.UNO.Modelos.Mesa;

public interface IObservable {
    public void agregarObservador(IObservador observador);
    public void notificar();
}
