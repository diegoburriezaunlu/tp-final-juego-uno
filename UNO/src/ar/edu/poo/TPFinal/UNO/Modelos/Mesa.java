package ar.edu.poo.TPFinal.UNO.Modelos;

import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.Carta;
import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.Color;
import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.ICarta;

import java.util.ArrayList;

public class Mesa {
    private ArrayList<ICarta> cartasManoDeJugador1 = new ArrayList<>();
    private ArrayList<ICarta> cartasManoDeJugador2 = new ArrayList<>();
    private ICarta cartaALaVista;
    private ArrayList<Integer> puntosDeJugadores = new ArrayList<>();
    private String turno;
    private String uno;
    private Color colorEnJuego;
    private ArrayList<Integer> cantDeCartasEnMano = new ArrayList<>();
    private String mensajesAJugadoresSiBajaCarta;
    private boolean pedirEleccionColorMesa;
    private int ganadorDelPartido=99;
    private int aQueJugadorVaDirigido; //Dice a que jugador va dirigido el mensaje de bajar la carta.(Jugador nro NO! index)

    public Mesa() {
        this.puntosDeJugadores.add(0);
        this.puntosDeJugadores.add(0);
        this.turno = "Jugador 1";
        this.uno = null;
        this.cantDeCartasEnMano.add(7);
        this.cantDeCartasEnMano.add(7);
        this.pedirEleccionColorMesa=false;
    }

    public ArrayList<ICarta> getCartasManoDeJugador(int jugador) {
        if (jugador == 0){
            return cartasManoDeJugador1;
        }else{
            return cartasManoDeJugador2;
        }
    }
    void setCartasManoDeJugador1(ArrayList<Carta> cartasManoDeJugador1) {
        this.cartasManoDeJugador1 = new ArrayList<ICarta>();
        this.cartasManoDeJugador1.addAll(cartasManoDeJugador1);
        this.cantDeCartasEnMano.add(0,cartasManoDeJugador1.size());
    }
    void setCartasManoDeJugador2(ArrayList<Carta> cartasManoDeJugador2) {
        this.cartasManoDeJugador2 = new ArrayList<ICarta>();
        this.cartasManoDeJugador2.addAll(cartasManoDeJugador2);
        this.cantDeCartasEnMano.add(1,cartasManoDeJugador2.size());
    }

    public boolean getPedirEleccionColorMesa() {
        return pedirEleccionColorMesa;
    }

    public void setPedirEleccionColorMesa(boolean pedirEleccionColorMesa) {
        this.pedirEleccionColorMesa = pedirEleccionColorMesa;
    }

    public ICarta getCartaALaVista() {
        return cartaALaVista;
    }

    void setCartaALaVista(Carta cartaALaVista) {
        this.cartaALaVista = cartaALaVista;
    }

    public int getPuntosDeJugadores(int jugador) {
        return puntosDeJugadores.get(jugador);
    }

    void setPuntosDeJugadores(ArrayList<Integer> puntosDeJugadores) {
        this.puntosDeJugadores = puntosDeJugadores;
    }

    public String getTurno() {
        return turno;
    }

    void setTurno(String turno) {
        this.turno = turno;
    }

    public String getUno() { return uno; }

    void setUno(String jugador) {
        this.uno=jugador;
    }
    public Color getColorEnJuego() {
        return colorEnJuego;
    }

    public void setColorEnJuego(Color colorEnJuego) {
        this.colorEnJuego = colorEnJuego;
    }
    public int getCantCartasEnMano(int jugador){
        return cantDeCartasEnMano.get(jugador-1);
    }
    public void setTurno(int jugador){
        if (jugador == 1){
            this.turno = "Jugador 1";
        }else if (jugador == 2){
            this.turno = "Jugador 2";
        }
    }
    public String getMensajesAJugadoresSiBajaCarta() {
        return mensajesAJugadoresSiBajaCarta;
    }
    public void setMensajesAJugadoresSiBajaCarta(String mensajesAJugadoresSiBajaCarta) {
        this.mensajesAJugadoresSiBajaCarta = mensajesAJugadoresSiBajaCarta;
    }

    public int getaQueJugadorVaDirigido() {
        return aQueJugadorVaDirigido;
    }

    public void setaQueJugadorVaDirigido(int aQueJugadorVaDirigido) {
        this.aQueJugadorVaDirigido = aQueJugadorVaDirigido;
    }

    public int getGanadorDelPartido() {
        return ganadorDelPartido;
    }

    public void setGanadorDelPartido(int ganadorDelPartido) {
        this.ganadorDelPartido = ganadorDelPartido;
    }
}
