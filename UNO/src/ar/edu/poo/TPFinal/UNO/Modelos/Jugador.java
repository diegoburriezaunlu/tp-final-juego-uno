package ar.edu.poo.TPFinal.UNO.Modelos;

import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.Carta;
import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.CartaAccion;

import java.util.ArrayList;

public class Jugador {
    private int puntos;
    private ArrayList<Carta> cartaEnMano =new ArrayList<Carta>();
    private boolean dijoUno=false;
    public Jugador() {
        this.puntos=0;
    }
    public void obtenerCartaParaMano(Carta carta){
        this.cartaEnMano.add(carta);
    }
    public Carta bajarCarta(int index){
        return this.cartaEnMano.remove(index);
    }
    public int getPuntos(){
        return this.puntos;
    }
    //Agrego los puntos ganados en la mano.
    public void sumarPuntos(int puntos){
        this.puntos += puntos;
    }
    public boolean esUltimaCarta(){
        if (cartaEnMano.size()==1){
            return true;
        } else {
            return false;
        }
    }
    public boolean terminoSusCartas(){
        return cartaEnMano.isEmpty();
    }
    public void setdecirUno(){
        this.dijoUno=true;
    }
    public boolean getdijoUno(){
        return this.dijoUno;
    }
    ArrayList<Carta> getCartaEnMano() {
        return cartaEnMano;
    }
    public void borrarDijoUno(){
        this.dijoUno=false;
    }
    public int cuantasCartasLeQuedanEnMano(){
        return this.cartaEnMano.size();
    }
    //Suma los puntos de las cartas que le quedaron en mano.
    public int sumaPuntosEnMano(){
        int suma=0;
        for (int i=0;i<cartaEnMano.size();i++){
            if (cartaEnMano.get(i) instanceof CartaAccion){
                suma += ((CartaAccion)cartaEnMano.get(i)).getPuntos();
            }
        }
        return suma;
    }
    //Cuando inicia nueva mano.
    public void nuevaMano(){
        this.cartaEnMano =new ArrayList<Carta>();
    }
}
