package ar.edu.poo.TPFinal.UNO.Modelos.Cartas;

public class CartaNumerica extends Carta implements ICarta{
    private int numero;
    private Color color;

    public CartaNumerica(int numero, Color color) {
        this.numero = numero;
        this.color = color;
    }

    public int getNumero() {
        return numero;
    }
    public Color getColor() {
        return color;
    }
    //verifica si la carta que bajo la puede bajar
    @Override
    public boolean verificarSiEstaPermitido(Carta cartaAnterior, Color colorEnJuego) {
        if(cartaAnterior instanceof CartaNumerica) {
            if (this.color.equals(colorEnJuego) || this.numero == ((CartaNumerica) cartaAnterior).getNumero()) {
                return true;
            }
        }else if (this.color.equals(colorEnJuego)){
            return true;
        }
        return false;
    }
    //Presenta la carta en String para mostrar en la mesa.
    public String mostrarCarta(){ return "* NUMERO "+this.numero+" --> "+color+" *"; }

}
