package ar.edu.poo.TPFinal.UNO.Modelos.Cartas;

import ar.edu.poo.TPFinal.UNO.Modelos.Jugador;

public class CartaDeEleccionColor extends CartaAccion implements ICarta{
    private Color color;
    private static final int puntos=50;

    public CartaDeEleccionColor(Color color) {
        this.color = color;
    }

    @Override
    public int getPuntos() {
        return this.puntos;
    }

    @Override
    public boolean verificarSiEstaPermitido(Carta cartaAnterior, Color colorEnJuego) {
        return true;
    }

    public String mostrarCarta(){ return "* ELECCION DE COLOR *"; }

    @Override
    public Color getColor() {
        return this.color;
    }
}
