package ar.edu.poo.TPFinal.UNO.Modelos.Cartas;

public abstract class Carta implements ICarta{
    public abstract boolean verificarSiEstaPermitido(Carta cartaAnterior, Color colorEnJuego);
    public abstract String mostrarCarta();
    public abstract Color getColor();
}
