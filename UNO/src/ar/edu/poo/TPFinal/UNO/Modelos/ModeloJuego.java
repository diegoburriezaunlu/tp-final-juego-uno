package ar.edu.poo.TPFinal.UNO.Modelos;

import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.*;
import ar.edu.poo.TPFinal.UNO.Observer.IObservable;
import ar.edu.poo.TPFinal.UNO.Observer.IObservador;

import java.util.ArrayList;
import java.util.Collections;

public class ModeloJuego implements IObservable {
    private ArrayList<Carta> mazo = new ArrayList<Carta>();
    private ArrayList<Carta> mazoUsado = new ArrayList<>();
    private Carta cartaALaVista;
    private Color colorEnJuego;
    private ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
    private Jugador turnoActual;
    private ArrayList<IObservador> observadores = new ArrayList<>();
    private Mesa mesa;
    private Jugador ganadorDelPartidoCompleto=null;
    private String mensajesAJugadoresSiBajaCarta = "";
    private boolean pedirColorDeMesa;
    private String sentidoDeRonda="+";


    public ModeloJuego() {
        mezclarMaso();
        mesa = new Mesa();
    }

    @Override
    public void agregarObservador(IObservador observador) {
        this.observadores.add(observador);
    }

    //Primero actualiza el objeto mesa y luego lo notifica a los observadores.
    @Override
    public void notificar() {
        actualizar();
        for(int i=0;i< observadores.size();i++){
            observadores.get(i).actualizar(mesa);
        }
    }
    public void iniciarJuego(int cantJugadores){
        jugadores.add(new Jugador());
        jugadores.add(new Jugador());
        repartirCartas(jugadores.get(0),jugadores.get(1));
        this.turnoActual=jugadores.get((int) (Math.random() * 2));

        //verifico si la carta que quedo a la vista es una carta de accion.
        if (this.cartaALaVista instanceof CartaAccion) {
            String opcion="4";
            while (this.cartaALaVista instanceof CartaAccion && opcion.equals("4")) {
                opcion = new Regla().queClaseEs(this.cartaALaVista);
                switch (opcion) {
                    case "1" -> {
                        setPedirColorDeMesa(true);
                    }
                    case "2" -> {
                        cambiarSentidoDeRonda();
                    }
                    case "3" -> {
                        cambiarTurno();
                    }
                    case "4" -> {
                        this.mazoUsado.add(cartaALaVista);
                        cartaALaVista = mazo.remove(0);
                        this.colorEnJuego = this.cartaALaVista.getColor();
                    }
                    case "5" -> {
                        conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                        conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                        cambiarTurno();
                    }
                }
            }
        }
        notificar();
    }
    private void actualizar(){
        this.mesa = new Mesa();

        //Cargo un array de cartas que tiene en mano, por cada jugador
        mesa.setCartasManoDeJugador1(jugadores.get(0).getCartaEnMano());
        mesa.setCartasManoDeJugador2(jugadores.get(1).getCartaEnMano());
        mesa.setCartaALaVista(cartaALaVista);

        //instancio la variable de mesa que lleva que jugador dijo UNO (Encaso que sea asi)
        if(jugadores.get(0).getdijoUno()){
            mesa.setUno("JUGADOR 1");
        }
        if(jugadores.get(1).getdijoUno()){
            mesa.setUno("JUGADOR 2");
        }

        // Seteo el color que se esta jugando
        mesa.setColorEnJuego(this.colorEnJuego);

        //asino un numero de jugador el cual es su turno
        mesa.setTurno(jugadores.indexOf(turnoActual)+1);

        //En este condicional aviso a la vista si tiene que preguntar si baja la carta que tomo
        //En caso afirmativo dice a que jugador va dirigido.
        if (!mensajesAJugadoresSiBajaCarta.isEmpty()) {
            mesa.setMensajesAJugadoresSiBajaCarta(this.mensajesAJugadoresSiBajaCarta);
            mesa.setaQueJugadorVaDirigido(jugadores.indexOf(this.turnoActual) + 1);
        }

        // Luego de avisar a la vista que tiene que preguntar si baja la carta, blanqueo la variable
        //de ModeloJuego para que no altere la proxima jugada.
        this.mensajesAJugadoresSiBajaCarta="";

        //Aviso que pida el color a jugar al jugador.
        mesa.setPedirEleccionColorMesa(this.pedirColorDeMesa);
        if (this.pedirColorDeMesa){
            mesa.setaQueJugadorVaDirigido(jugadores.indexOf(this.turnoActual) + 1);
        }

        //Cuando al mazo le queda solo 4 cartas, mezclo el mazo usado y se lo agrego al maso.
        if(mazo.size()==4){
            Collections.shuffle(mazoUsado);
            mazo.addAll(mazoUsado);
            mazoUsado = new ArrayList<>();
        }

        //Aviso si ya hay un ganador del partido
        if(this.ganadorDelPartidoCompleto!=null){
            mesa.setGanadorDelPartido(jugadores.indexOf(this.ganadorDelPartidoCompleto)+1);
        }
    }
    private void repartirCartas(Jugador jugador1, Jugador jugador2){
        for (int i=0;i<7;i++){
            jugador1.obtenerCartaParaMano(mazo.remove(0));
            jugador2.obtenerCartaParaMano(mazo.remove(0));
        }
        cartaALaVista = mazo.remove(0);
        this.colorEnJuego=this.cartaALaVista.getColor();
    }
    public boolean getPedirColorDeMesa() {
        return pedirColorDeMesa;
    }

    public void setPedirColorDeMesa(boolean pedirColorDeMesa) {
        this.pedirColorDeMesa = pedirColorDeMesa;
    }

    private void tomarUnaCartaDeLaPila(Jugador jugador){
        jugador.obtenerCartaParaMano(mazo.remove(0));
    }

    //Hago una sobrecarga para usarla desde controlador que no conoce a los jugadores.
    public void tomarUnaCartaDeLaPila(int jugador) {
        //verifico que sea su turno.
        if (jugadores.get(jugador - 1).equals(turnoActual)) {
            //Verifico si algun jugador le queda solo una carta y no dijo UNO.
            for (int i = 0; i < jugadores.size(); i++) {
                if (jugadores.get(i).cuantasCartasLeQuedanEnMano() == 1 && !jugadores.get(i).getdijoUno()) {
                    jugadores.get(i).obtenerCartaParaMano(mazo.remove(0));
                }
            }
            //Antes de tomar una carta verifico que el jugador no haya dicho UNO.
            if (jugadores.get(jugador - 1).getdijoUno()) { //Si dijo UNO
                jugadores.get(jugador - 1).obtenerCartaParaMano(mazo.remove(0));
                jugadores.get(jugador - 1).borrarDijoUno();
                cambiarTurno();
            } else {
                //si no habia dicho UNO
                this.mensajesAJugadoresSiBajaCarta = "¿Bajas la carta que tomaste?";
                jugadores.get(jugador - 1).obtenerCartaParaMano(mazo.remove(0));
            }
            //desactivo la opcion de los jugadores que dijeron UNO
            for (int i = 0; i < jugadores.size(); i++) {
                if (i != jugador) {
                    jugadores.get(i).borrarDijoUno();
                }
            }
        }else {
            jugadores.get(jugador - 1).obtenerCartaParaMano(mazo.remove(0));
        }
        notificar();
    }

    public void bajarUnaCarta(int jugador, int index){
        boolean respuesta=false;
        Carta anteriorCartaALaVista = this.cartaALaVista;
        //verifico que sea su turno.
        if (jugadores.get(jugador-1).equals(turnoActual)) {
            //verifico si puede jugar esa carga
            if (new Regla().verificarJugada(anteriorCartaALaVista, jugadores.get(jugador-1).getCartaEnMano().get(index), colorEnJuego)) {
                //Bajo la carta.
                cambioCartaEnMesa(jugadores.get(jugador-1).bajarCarta(index));
                //Verifico si no canto UNO.
                verificoSiNoCantoUno(jugador);
                //Verifico si el jugador termino sus cartas de la mano.
                //Si no termino sigo el proceso.
                if (!jugadores.get(jugador-1).terminoSusCartas()) {
                    //verifico si es una carta de accion para aplicar accion correspondiente.
                    if (this.cartaALaVista instanceof CartaAccion) {
                        String opcion = new Regla().queClaseEs(this.cartaALaVista);
                        switch (opcion) {
                            case "1" -> {
                                setPedirColorDeMesa(true);
                            }
                            case "2" -> {
                                cambiarSentidoDeRonda();
                                cambiarTurno();
                            }
                            case "3" -> {
                                cambiarTurno();
                                cambiarTurno();
                            }
                            case "4" -> {
                                conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                                conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                                conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                                conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                                setPedirColorDeMesa(true);
                            }
                            case "5" -> {
                                conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                                conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                                cambiarTurno();
                                cambiarTurno();
                            }
                        }
                    } else {
                        //Si no es carta de accion, cambio el turno.
                        cambiarTurno();
                    }
                }else{
                    //Si termino sus carta, hago la suma y reinicio la partida.
                    new Regla().contarPuntos(jugadores);//suma los puntos de la partida.
                    //Busco si ya hay un ganador del juego.
                    buscoGanadorDePartido();
                    //reparto las cartas nuevamente una nueva partida.
                    repartirCartas(jugadores.get(0),jugadores.get(1));
                    this.turnoActual=jugadores.get((int) (Math.random() * 2));
                    //verifico si la carta que quedo a la vista es una carta de accion.
                    if (this.cartaALaVista instanceof CartaAccion) {
                        String opcion="4";
                        while (this.cartaALaVista instanceof CartaAccion && opcion.equals("4")) {
                            opcion = new Regla().queClaseEs(this.cartaALaVista);
                            switch (opcion) {
                                case "1" -> {
                                    setPedirColorDeMesa(true);
                                }
                                case "2" -> {
                                    cambiarSentidoDeRonda();
                                }
                                case "3" -> {
                                    cambiarTurno();
                                }
                                case "4" -> {
                                    this.mazoUsado.add(cartaALaVista);
                                    cartaALaVista = mazo.remove(0);
                                    this.colorEnJuego = this.cartaALaVista.getColor();
                                }
                                case "5" -> {
                                    conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                                    conocerProximoTurno().obtenerCartaParaMano(mazo.remove(0));
                                    cambiarTurno();
                                }
                            }
                        }
                    }
                }
            }else {
                //Si no podia jugar esa carta, toma una como pena y pierde pasa el turno.
                tomarUnaCartaDeLaPila(jugadores.get(jugador-1));
                cambiarTurno();
            }
        }else{
            //Si no era su turno, levanta una carta del mazo.
            tomarUnaCartaDeLaPila(jugadores.get(jugador-1));
        }
        notificar();
    }

    //Busco ganador del partido Completo
    public void buscoGanadorDePartido(){
        for (int i=0;i<jugadores.size();i++){
            if (jugadores.get(i).getPuntos()>=500){
                this.ganadorDelPartidoCompleto = jugadores.get(i);
            }
        }
    }
    public boolean hayGanadorDePartido(){
        if (this.ganadorDelPartidoCompleto==null){
            return false;
        }else{
            return true;
        }
    }
    //Verifico si le queda una carta y no dijo UNO, lo multo con una del Mazo.
    public void verificoSiNoCantoUno(int jugador){
        if(jugadores.get(jugador-1).cuantasCartasLeQuedanEnMano()==1&&!jugadores.get(jugador-1).getdijoUno()){
            jugadores.get(jugador-1).obtenerCartaParaMano(mazo.remove(0));
        }
    }
    public void cambioCartaEnMesa(Carta carta){
        this.mazoUsado.add(this.cartaALaVista);
        this.cartaALaVista = carta;
        this.colorEnJuego = this.cartaALaVista.getColor();
    }
    private void cambiarTurno(){
        int opc = jugadores.indexOf(turnoActual);
        if (this.sentidoDeRonda.equals("+")) {
            if (jugadores.size() == opc + 1) {
                this.turnoActual = jugadores.get(0);
            } else {
                this.turnoActual = jugadores.get(opc + 1);
            }
        }
        if (this.sentidoDeRonda.equals("-")) {
            if (opc == 0) {
                this.turnoActual = jugadores.get(jugadores.size() - 1);
            } else {
                this.turnoActual = jugadores.get(opc - 1);
            }
        }
    }
    private Jugador conocerProximoTurno(){
        Jugador respuesta = null;
        int opc = jugadores.indexOf(turnoActual);
        if (this.sentidoDeRonda.equals("+")) {
            if (jugadores.size() == opc + 1) {
                respuesta = jugadores.get(0);
            } else {
                respuesta = jugadores.get(opc + 1);
            }
        }
        if (this.sentidoDeRonda.equals("-")) {
            if (opc == 0) {
                respuesta = jugadores.get(jugadores.size() - 1);
            } else {
                respuesta = jugadores.get(opc - 1);
            }
        }
        return respuesta;
    }
    public void pasarTurno(int cantidadDeTurnos){
        if(cantidadDeTurnos==2){
            cambiarTurno();
            cambiarTurno();
            notificar();
        }else {
            cambiarTurno();
            notificar();
        }
    }
    private void cambiarSentidoDeRonda(){
        if (this.sentidoDeRonda.equals("+")){
            this.sentidoDeRonda="-";
        }else{
            this.sentidoDeRonda="+";
        }
    }
    public void decirUno(int jugador){
        //Primero verifica que sea su turno.
        if(this.turnoActual.equals(jugadores.get(jugador-1))) {
            //Luego verifica que le queden 2 cartas.
            if (jugadores.get(jugador - 1).cuantasCartasLeQuedanEnMano() == 2) {
                jugadores.get(jugador - 1).setdecirUno();
            } else {
                jugadores.get(jugador - 1).obtenerCartaParaMano(mazo.remove(0));
                cambiarTurno();
            }
        }else{
            jugadores.get(jugador - 1).obtenerCartaParaMano(mazo.remove(0));
        }
        notificar();
    }
    private boolean dijoUno(Jugador jugador){
        return jugador.getdijoUno();
    }
    private void desafiarJugador(){

    }
    public void setColorEnJuego(Color colorEnJuego) {
        this.colorEnJuego = colorEnJuego;
    }
    public void setColorEnJuego(String color) {
        setColorEnJuego(Color.valueOf(color) );
    }

    private void mezclarMaso(){
        for (int j =1;j<3;j++){
            for (int i = 1;i<10;i++){
                mazo.add(new CartaNumerica(i, Color.AMARILLO));
                mazo.add(new CartaNumerica(i, Color.VERDE));
                mazo.add(new CartaNumerica(i, Color.AZUL));
                mazo.add(new CartaNumerica(i, Color.ROJO));
            }
            mazo.add(new CartaDeRetorno(Color.AMARILLO));
            mazo.add(new CartaDeRetorno(Color.VERDE));
            mazo.add(new CartaDeRetorno(Color.AZUL));
            mazo.add(new CartaDeRetorno(Color.ROJO));

            mazo.add(new CartaDeSaltoTurno(Color.AMARILLO));
            mazo.add(new CartaDeSaltoTurno(Color.VERDE));
            mazo.add(new CartaDeSaltoTurno(Color.AZUL));
            mazo.add(new CartaDeSaltoTurno(Color.ROJO));

            mazo.add(new CartaDeTomaDos(Color.AMARILLO));
            mazo.add(new CartaDeTomaDos(Color.VERDE));
            mazo.add(new CartaDeTomaDos(Color.AZUL));
            mazo.add(new CartaDeTomaDos(Color.ROJO));

            mazo.add(new CartaDeEleccionColor(Color.MULTICOLOR));
            mazo.add(new CartaDeEleccionColor(Color.MULTICOLOR));

            mazo.add(new CartaDeTomaCuatroYEleccionColor(Color.MULTICOLOR));
            mazo.add(new CartaDeTomaCuatroYEleccionColor(Color.MULTICOLOR));
        }
        Collections.shuffle(mazo);
    }

}
