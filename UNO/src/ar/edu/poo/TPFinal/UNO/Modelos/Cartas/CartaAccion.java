package ar.edu.poo.TPFinal.UNO.Modelos.Cartas;

import ar.edu.poo.TPFinal.UNO.Modelos.Jugador;

public abstract class CartaAccion extends Carta implements ICarta {
    public abstract boolean verificarSiEstaPermitido(Carta cartaAnterior, Color colorEnJuego);

    public abstract int getPuntos();
    public abstract String mostrarCarta();
    public abstract Color getColor();
}
