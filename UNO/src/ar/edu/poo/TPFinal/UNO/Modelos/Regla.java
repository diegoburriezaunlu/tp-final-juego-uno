package ar.edu.poo.TPFinal.UNO.Modelos;

import ar.edu.poo.TPFinal.UNO.Modelos.Cartas.*;

import java.util.ArrayList;

public class Regla {
    public boolean verificarJugada(Carta cartaAnterior, Carta cartaActual, Color colorEnJuego){
        //por polimorfismo verifica si puede bajar esa carta.
        if (cartaActual.verificarSiEstaPermitido(cartaAnterior, colorEnJuego)){
            return true;
        }
        return false;
    }
    public String queClaseEs(Carta carta){
        String respuesta="";
        if (carta instanceof CartaDeEleccionColor){ respuesta="1"; }
        if (carta instanceof CartaDeRetorno){ respuesta="2"; }
        if (carta instanceof CartaDeSaltoTurno){ respuesta="3"; }
        if (carta instanceof CartaDeTomaCuatroYEleccionColor){ respuesta="4"; }
        if (carta instanceof CartaDeTomaDos){ respuesta="5"; }
        return respuesta;
    }
    public void contarPuntos(ArrayList<Jugador> jugadores){
        int sumaPuntaje=0;
        Jugador ganador=new Jugador();//Lo instancie para que compile pero luego va a estar el ganador.
        for (int i=0;i<jugadores.size();i++){
            if (jugadores.get(i).terminoSusCartas()){
                ganador = jugadores.get(i);
                jugadores.get(i).nuevaMano();//Reseteo el jugador
            }else{
                sumaPuntaje += jugadores.get(i).sumaPuntosEnMano();
                jugadores.get(i).nuevaMano();//Reseteo el jugador
            }
        }
        ganador.sumarPuntos(sumaPuntaje);
    }
}
