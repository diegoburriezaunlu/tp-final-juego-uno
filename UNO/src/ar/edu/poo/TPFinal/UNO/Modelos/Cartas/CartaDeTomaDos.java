package ar.edu.poo.TPFinal.UNO.Modelos.Cartas;

public class CartaDeTomaDos extends CartaAccion implements ICarta{
    private Color color;
    private static final int puntos=20;

    public CartaDeTomaDos(Color color) {
        this.color = color;
    }

    @Override
    public boolean verificarSiEstaPermitido(Carta cartaAnterior, Color colorEnJuego) {
        boolean respuesta=false;
        //verifica si el color de la carta que baja es igual al color del juego o
        //si es igual a la que estaba en mesa.
        if (this.color.equals(colorEnJuego) || cartaAnterior instanceof CartaDeTomaDos){
            respuesta=true;
        }
        return respuesta;
    }
    public int getPuntos() {
        return this.puntos;
    }
    public String mostrarCarta(){ return "* TOMA +2 --> "+color+" *"; }

    @Override
    public Color getColor() {
        return this.color;
    }
}
