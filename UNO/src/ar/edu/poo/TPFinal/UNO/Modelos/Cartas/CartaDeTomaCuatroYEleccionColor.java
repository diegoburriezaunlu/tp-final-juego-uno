package ar.edu.poo.TPFinal.UNO.Modelos.Cartas;

public class CartaDeTomaCuatroYEleccionColor extends CartaAccion implements ICarta{
    private Color color;
    private static final int puntos=50;

    public CartaDeTomaCuatroYEleccionColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean verificarSiEstaPermitido(Carta cartaAnterior, Color colorEnJuego) {
        return true;
    }
    public int getPuntos() {
        return this.puntos;
    }
    public String mostrarCarta(){ return "* TOMA +4 --> "+color+" *"; }

    @Override
    public Color getColor() {
        return this.color;
    }
}
