import ar.edu.poo.TPFinal.UNO.Controladores.ControladorConsola;
import ar.edu.poo.TPFinal.UNO.Modelos.ModeloJuego;
import ar.edu.poo.TPFinal.UNO.Vistas.VistaDeConsola.VistaConsola;

public class Juego {
    public static void main(String[] args) {
        ModeloJuego modelo =new ModeloJuego();
        for (int i=1;i<3;i++){
            new ControladorConsola(new VistaConsola(i),modelo,i);
        }
        modelo.iniciarJuego(2);
    }
}